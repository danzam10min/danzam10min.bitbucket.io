# Un sito in 10 minuti

## Scopo
Questo è il sito che illustra, in poco tempo, 
come creare un sito statico con:

* Sphinx,
* git,
* bitbucket.

E come farlo ospitare da bitbucket stesso.


## Repository

Il repository che contiene tutto questo lavoro lo si può vedere, 
scaricare o clonare in:

[bitbucket.org/danzam10min/sito10min]
(https://bitbucket.org/danzam10min/sito10min/)

## Sito

Il sito prodotto è visibile in:

[danzam10min/bitbucket.io/](https://danzam10min/bitbucket.io/)

Buon divertimento.
